<%--
  Created by IntelliJ IDEA.
  User: lukasz
  Date: 03.10.17
  Time: 18:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dodawanie nowej pozycji</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<div style="float: left; width: 30%;">
    <%@ include file="../menu.jsp" %>
</div>
<div style="float: right; width: 70%;">
    <form name="bookForm" action="/addBook" method="post">
        <label>Nazwa ksiażki</label><input type="text" name="title"/>
        <label>Liczba stron</label><input type="text" name="pageCount"/>
        <label>ISBN</label><input type="text" name="isbn"/>
        <label>Author Name</label><input type="text" name="author.name"/>
        <label>Author Surname</label><input type="text" name="author.surname"/>
        <select name="genre">
            <c:forEach items="${BookRepository}" var="elem">
                <option value="${elem}"> ${elem.getFriendlyName()} </option>
            </c:forEach>
        </select>


        <input type="submit" value="Zapisz"/>
    </form>
</div>

</body>
</html>
