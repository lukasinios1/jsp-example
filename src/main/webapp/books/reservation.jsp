<%--
  Created by IntelliJ IDEA.
  User: lukasz
  Date: 03.10.17
  Time: 18:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dodawanie nowej pozycji</title>
</head>
<body>
<%@ include file="../header.jsp" %>

<div style="float: left; width: 30%;">
    <%@ include file="../menu.jsp" %>
</div>
<div style="float: right; width: 70%;">

    <c:if test="${book eq null}">
        <jsp:forward page="../error.jsp"/>
    </c:if>

    <p>Wypożycz książkę - ${book.getTitle()}</p>
    <p>Podaj dane osobowe</p>
    <%@ include file="rentBook.jsp"%>


</div>

</body>
</html>
