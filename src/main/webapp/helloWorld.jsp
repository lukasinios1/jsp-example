<%@ page import="java.time.LocalDateTime" %>
<%@ page import="com.jspi.RandomNumberGenerator" %>
<%@ page import="java.util.Random" %>
<%@ page import="java.util.stream.Stream" %>
Created by IntelliJ IDEA.
User: lukasz
Date: 29.09.17
Time: 18:15
To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Helo world<%= LocalDateTime.now()%>
</h1>
<a href="helloWorld.jsp">Click Me!</a>
<p>zawartość żądania: </p>
<p>
    Cookies: <%= request.getCookies()%> <br>
    Host:   <%= request.getHeader("Host")%> <br>
    aUTHtYPE:  <%= request.getAuthType()%> <br>
    ContextPath:  <%= request.getContextPath()%> <br>
    Method:  <%= request.getMethod()%> <br>
    <%--Parts:  <%= request.getParts()%> <br>--%>
    <%--PathTrans:  <%= request.getPathTranslated()%> <br>--%>
    Remoteuser:  <%= request.getRemoteUser()%> <br>
    resposne Headers:  <%= response.getHeaderNames()%> <br>
    response status:  <%= response.getStatus()%><br>

    numery totka: <%= RandomNumberGenerator.drowLottoNumbers()%> <br/>
    numery Euro:
    <%
        Random random = new Random();
        StringBuilder z45 = new StringBuilder();
        StringBuilder z10 = new StringBuilder();

        for (int i = 0; i < 5; i++) {
            if (i != 0) {
                z45.append(", ");
                if (i < 2) {
                    z10.append(", ");
                }
            }
            if (i < 2) {
                z10.append(random.nextInt(10) + 1);
            }
            z45.append(random.nextInt(45) + 1);
        }
        out.println(z45);
        out.println("<br/> oraz 2 z 10: ");
        out.println(z10);

        String userParams = request.getQueryString();
        if (!userParams.contains("dzialanie") & !userParams.contains("liczba1") & userParams.contains("dzialanie")) {
            response.sendError(400, "Missing statment");
        } else {
            String[] params = userParams.split("&");
            String idParam = null;
//                String idParam = Stream.of(params)
//                        .filter(param -> param.contains("id"))
//                        .findAny().orElse(null);

            for (String param : params) {
                String[] keyValuePair = param.split("=");
                if (keyValuePair.length == 2 && keyValuePair[0].contains("id")) {
                    idParam = keyValuePair[1];
                    break;
                }
            }
            if (idParam == null) {
                response.sendError(404);
            } else {
                out.println("Wartość paramteru id: " + idParam);
            }
        }

    %>

</p>
</body>
</html>
