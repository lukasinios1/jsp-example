<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
  Created by IntelliJ IDEA.
  User: lukasz
  Date: 02.10.17
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%@ include file="header.jsp" %>
<%-- Komentarz nie widoczny --%>
<!-- Komentarz widoczny -->
<%! int requestCount = 0; %>
<%--<%! Book books = BookGenerator.createBook("a", "a", "a"); %>--%>

<div>
    <div style="float: left; width: 30%;">
        <%@ include file="menu.jsp" %>
    </div>
    <div style="float: right; width: 70%;">
        <p>Content strony</p>
        <table>
            <c:forEach items="${list}" var="book">
                <tr>
                    <td>${book.getTitle()}</td>
                    <td>${book.getAuthor().toString()}</td>
                    <td>${book.getPageCount()}</td>
                    <td><a href="/books/reservation?bookId=${book.getId()}">Zarezewuj</a></td>
                </tr>
            </c:forEach>
        </table>
        <p> Ilosc wywołań strony: <%= ++requestCount%>
        </p>
        <div style="clear: both"></div>
    </div>


</div>
</body>
</html>
