<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: lukasz
  Date: 03.10.17
  Time: 17:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:forEach items="${menu}" var="elem">
    <a href="${elem.getUrl()}">
        <li>${elem.getElementName()}</li>
    </a>
</c:forEach>
<%--<ul>--%>
<%--<li>Strona główna</li>--%>
<%--<li>Dodawanie książek</li>--%>
<%--<li>Lista książek</li>--%>
<%--<li>Wypożyczenie książek</li>--%>
<%--</ul>--%>

