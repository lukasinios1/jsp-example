package com.jspi;

import com.jspi.library.books.Author;
import com.jspi.library.books.Book;
import com.jspi.library.books.BookGenre;

import java.util.ArrayList;
import java.util.Random;

public class BookGenerator {
    private static final String[] COUNTRY_CODES = new String[]{"PL", "DE", "UK", "US", "RU"};

    public static Book createBook(String title, String authorName, String authorSurname) {

        Random random = new Random();
        BookGenre[] values = BookGenre.values();

        Author author = new Author(
                authorName,
                authorSurname,
                COUNTRY_CODES[random.nextInt(COUNTRY_CODES.length)]);

        Book book = new Book(
                title,
                generateISBN(random),
                random.nextInt(1500),
                author,
                values[random.nextInt(values.length)]);

        book.setId(RandomNumberGenerator.getNextBookSequence());

        return book;
    }

    private static String generateISBN(Random random) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    public static ArrayList<Book> generateRandomBooks(int booksQuantity) {
        Random random = new Random();
        names[] names = BookGenerator.names.values();
        surnames[] surnames = BookGenerator.surnames.values();
        titles[] titles = BookGenerator.titles.values();
        ArrayList<Book> list = new ArrayList<>();
        for (int i = 0; i < booksQuantity; i++) {
            list.add(
                    createBook(titles[random.nextInt(titles.length)].toString(),
                            names[random.nextInt(names.length)].toString(),
                            surnames[random.nextInt(surnames.length)].toString()));

        }
        return list;
    }

    public static ArrayList<Book> generateReallyRandomBooks(int booksQuantity) {
        ArrayList<Book> list = new ArrayList<>();
        for (int i = 0; i < booksQuantity; i++) {
            list.add(createBook(genreString(), genreString(), genreString()));
        }
        return list;
    }

    private static String genreString() {

        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < random.nextInt(20); i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    private enum names {
        Ada, Adela, Adelajda, Adrianna, Agata, Agnieszka, Aldona, Aleksandra, Alicja, Alina, Amanda, Amelia, Anastazja, Andżelika,
        Aneta, Edward, Emanuel, Emil, Eryk, Eugeniusz;
    }

    private enum surnames {
        Kowal, Man, Stajuda, Chabir;
    }

    private enum titles {
        Inferno, Harry_Potter, Java_Podstawy, Imie_Wiatru, Zwiadowcy;
    }

}
