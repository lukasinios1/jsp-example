package com.jspi.repository;

import com.jspi.RandomNumberGenerator;
import com.jspi.members.Member;

import java.util.ArrayList;
import java.util.List;

public class MemberRepository {
    private static List<Member> list = new ArrayList<>();

    public static void addMember(Member member) {
        member.setId(RandomNumberGenerator.getNextBookSequence());
        list.add(member);

    }

    public static Member getMemberById(String id) {
        if (id == null) {
            return null;
        }
        Member result = list.stream().filter(member -> Integer.toString(member.getId()).equals(id)).findAny().orElse(null);
        return result;
    }
}
