package com.jspi.repository;

import com.jspi.RandomNumberGenerator;
import com.jspi.library.books.Book;

import java.util.ArrayList;
import java.util.List;


public class BookRepository {
    private static List<Book> list = new ArrayList<>();

    public static void addBook(Book book) {
        book.setId(RandomNumberGenerator.getNextBookSequence());
        list.add(book);
    }

    public static Book getBookById(String id) {
        if (id == null) {
            return null;
        }
        Book result = list.stream().filter(book -> Integer.toString(book.getId()).equals(id)).findAny().orElse(null);
        return result;
    }

    public static List<Book> getBooks() {
        return list;
    }


}
