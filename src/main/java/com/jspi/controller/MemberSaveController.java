package com.jspi.controller;


import com.jspi.members.Member;
import com.jspi.repository.MemberRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MemberSaveController {

    @RequestMapping(value = "/rentBook", method = RequestMethod.POST)
    public String addMembers(@ModelAttribute("addMember")Member member){
        System.out.println(member.toString());
        System.out.println(member.getId());
        MemberRepository.addMember(member);
        return ("redirect:/");
    }
}
