package com.jspi.controller;


import com.jspi.library.books.BookGenre;
import com.jspi.menu.MenuElements;
import com.jspi.model.MenuModelAndView;
import com.jspi.repository.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BookListController {

    @RequestMapping(value = "/books/reservation")
    public ModelAndView bookReservationPage(
            @RequestParam(name = "bookId") String id){
        ModelAndView model = new MenuModelAndView();
        model.setViewName("reservation.jsp");
        model.addObject("book",BookRepository.getBookById(id));

        return model;
    }


    @RequestMapping(value = "/books/add")
    public ModelAndView addBookWebPage(){
        BookGenre[] bookGenres = BookGenre.values();
        MenuModelAndView model = new MenuModelAndView();
        model.setViewName("addBook.jsp");
        model.addObject("BookRepository", bookGenres);
        return model;
    }

    @RequestMapping(value = "/")
    public ModelAndView startPage(){
        MenuModelAndView model = new MenuModelAndView();
        model.setViewName("index.jsp");
        return model;
    }

    @RequestMapping(value = "/books")
    public ModelAndView getBooks() {
        ModelAndView model = new ModelAndView();
//        List<Book> list = BookGenerator.generateRandomBooks(5);

        model.addObject("menu", MenuElements.values());
        model.addObject("list", BookRepository.getBooks());
        model.setViewName("BookList.jsp");
        return model;
    }

    @RequestMapping(value = "/rentBook")
    public ModelAndView addMember(){
        ModelAndView model = new MenuModelAndView();
        model.setViewName("rentBook.jsp");
        return model;
    }


}
