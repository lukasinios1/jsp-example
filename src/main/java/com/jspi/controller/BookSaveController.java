package com.jspi.controller;

import com.jspi.library.books.Book;
import com.jspi.repository.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BookSaveController {

    @RequestMapping(value = "/addBook", method = RequestMethod.POST)
    public String addBook(@ModelAttribute("bookForm") Book book){
        System.out.println(book.getTitle());
        System.out.println(book.getPageCount());
        System.out.println(book.getIsbn());
        System.out.println(book.toString());
        BookRepository.addBook(book);
        return "redirect:/";

    }
}
