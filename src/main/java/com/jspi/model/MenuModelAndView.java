package com.jspi.model;

import com.jspi.menu.MenuElements;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

public class MenuModelAndView extends ModelAndView {

    public MenuModelAndView() {
        super();
        addObject("menu", MenuElements.values());
    }
}
