package com.jspi.menu;

import org.springframework.web.servlet.ModelAndView;

public enum MenuElements  {
    INDEX("Strona główna", "/"),
    ADD_BOOKS("Dodawanie książek", "/books/add"),
    BOOKS_LIST("Lista książek", "/books"),
    BOOK_RESERVATION("Wypożyczenie książek", "/books/reservation");


    public String getElementName() {
        return elementName;
    }

    public String getUrl() {
        return url;
    }

    MenuElements(String elementName, String url) {
        this.elementName = elementName;
        this.url = url;

    }

    private String elementName;
    private String url;
}
