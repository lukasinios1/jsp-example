package com.jspi;

import java.util.Random;


public class RandomNumberGenerator {

    private static int ID_BOOK_SEQUENCE = 0;
    private static int ID_MEMBER_SEQUENCE = 0;

    public static int getNextBookSequence(){
        return ++ID_BOOK_SEQUENCE;
    }

    public static int getNextIdMemberSequence(){
        return ++ID_MEMBER_SEQUENCE;
    }

    public static String drowLottoNumbers(){
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i <6 ; i++) {
            sb.append(random.nextInt(49)+1).append(", ");
        }
        return sb.toString();
    }

}
